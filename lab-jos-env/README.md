В этой задаче нужно будет добавить в JOS пользовательские процессы, которые в JOS называются *окружениями* (environment).
Нужно будет написать код инициализации и загрузки *окружения* в память, передачу управления, обработку исключений и системные вызовы.
В результате вы сможете запускать *окружения*, правда пока только в однозадачном режиме (поддержка многозадачности появится только в следующей лабораторной).

## Подготовка

Предполагается, что вы уже справились с задачей `lab-jos-mm` и код работы с виртуальной памятью у вас уже есть.
Начните с того, что скопируйте ваш предыдущий патч для JOS. Находясь в директории `lab-jos-mm` выполните 
```
git diff . > ../lab-jos-env/jos-mm.patch
```
После чего перейдите в `lab-jos-env` и скажите
```
patch -p2 < jos-mm.patch
```
Параметр `-p` указывает глубину текущей директории в пути, который указан в патче (git diff всегда пишет путь от корня репозитория).

По сравнению с `lab-jos-mm` в JOS добавились следующие файлы:

```

inc/
    env.h       Объявления для пользовательских окружений
    trap.h      Объявления для поддержки прерываний
    syscall.h   Объявления для поддерки системных вызовов
    lib.h       Объявления для библиотеки поддержки пользовательского режима
kern/
    env.h       Объявления пользовательских окружений, используемые внутри ядра
    env.c       Ядерная имплементация пользовательских окружений
    trap.h      Объявления для поддержки прерываний, используемые внутри ядра
    trap.c      Реализация прерываний
    trapentry.S Точки входа в прерывания, написанные на ассемблере
    syscall.h   Объявления для поддержки системных вызовов, используемые внутри ядра
    syscall.c   Реализация системных вызовов.
lib/
    entry.S     Точка входа в пользовательское окружение
    libmain.c   Пользовательский код инициализации, куда делается переход из entry.S
    syscall.c   Пользовательские затычки для системных вызовов
    console.c   Пользовательские функции putchar и getchar
    exit.c      Пользовательская функция exit
    panic.c     Пользовательская функция panic
user/
    *   Разные пользовательские программы, которыми можно протестировать код
```

## Описание пользовательских окружений

Объявления для окружений находится в файле `inc/env.h`. Посмотрите его. Каждому окружению в системе соответствует объект структуры `Env`.
Вам нужно будет написать код, который поддерживает сразу много окружений, хотя выполнить пока можно будет только одно.

В `kern/env.c` содеждатся глобальные переменные, которые позволяют JOS описывать все пользовательские окружения в системе

```
struct Env *envs = NULL;            // Массив всех окружений
struct Env *curenv = NULL;          // Окружение, которое сейчас выполняется на процессоре
static struct Env *env_free_list;   // Связный список свободных структур под окружения
```

При загрузке JOS инициализирует `envs`, чтобы эта переменная указывала на массив из `NENV` структур типа `Env`
(в каждый момент времени в JOS может работать не более `NENV` пользовательских окружений)

Свободные объекты из `envs` (т.е. те, которые не соответствуют никакому запущенному пользовательскому окружению), содержатся в односвязном списке, на который указывает `env_free_list`

Окружение, которое в данный момент работает на процессоре хранится в `curenv`. Это потребуется при обработке исключений, а также системных вызовов.

Окружения описываются структурой `Env`:

```
struct Env {
    struct Trapframe env_tf;    // Сохранённые регистры пользовательского кода, в формате, описаном в inc/trap.h.
    struct Env *env_link;       // Указатель на следующюу свободную Env в списке свободных окружений.
    envid_t env_id;             // Уникальный инедтефикатор
    envid_t env_parent_id;      // Идентефикатор родителя
    enum EnvType env_type;      // Тип окружения. Как правило здесь будет  ENV_TYPE_USER.
    unsigned env_status;        // Статус (окружение свободно, исполняется, готово к исполнению, ждёт, или умирает).
    uint32_t env_runs;          // Счётчик, сколько раз окружение было запущено.

    // Address space
    pde_t *env_pgdir;           // Ядерный виртуальный адрес, указывающий на Page Directory для окружения
};
```


## Аллокация массива под пользовательские окружения

Для начала нужно модифицировать `mem_init()` из `kern/pmap.c` чтобы аллоцировать массив `envs`. В нём дожно быть `NENV` объектов типа `struct Env`.
Также, как и `pages`, `envs` нужно замапить по адресу `UENVS` с доступом на чтение для пользовательского режима.
Убедитесь, что проходит тест `check_kern_pgdir()`.

## Создание пользовательских окружений

В этой части нужно заполнить пробелы в `kern/env.c`, после чего можно будет создать и запустить пользовательское окружение (правда, пока без шансов вернуться назад в ядро).

```
env_init() - Инициализирует все объекты Env и добавьте их в список env_free_list.
             Также вызывает env_init_percpu, которая заполняет таблицу дескрипторов
             сегментов сегментами для ядра и пользовательского кода.

env_setup_vm() - Аллоцирует таблицы страниц под новое пользовательское окружение
                 и инициализирует ядерную часть адресного пространства.

region_alloc() - Выделяет пользовательскую физическую память

load_icode() - Загружает пользовательский код в формате ELF в память,
               чтобы подготовить его к исполнению

env_create() - Выделяет новое пользовательское окружение и загружает код с помощью load_icode()

env_run() - переходит в пользовательский режим и передаёт управление окружению
```

## Запуск пользовательских окружений

В JOS ещё нет файловой системы. Как рассказывалось на семинаре, код пользовательских программ линкуется прямо в бинарник ядра, но специальным образом.
Чтобы загрузить окружение, используется макрос `ENV_CREATE`. Например, в функции `i386_init` в `kern/init.c` есть вызов

```
ENV_CREATE(user_hello, ENV_TYPE_USER);
```

В результате будет вызвана функция `env_create()`, а её параметр `binary` будет указывать на область памяти в которой лежит бинарный файл `obj/user/hello`, скомпилированный из `user/hello.c`.
Файл `obj/user/hello` сам по себе является файлом в формате ELF, поэтому `env_create()` должна загрузить его по тем адресам, которые прописаны в заголовке ELF.
Любопытно, что загрузчик JOS (`boot/main.c`), загружает ядро аналогичным образом, поэтому если у вас возникнут вопросы как правильно загружать код ELF в память, посмотрите на загрузчик.

Точка входа в программу также определена в заголовке ELF. Вам нужно будет сделать так, чтобы при переходе в пользовательское пространство регистр `eip` указывал на точку входа.

К этому моменту у вас должно получиться перейти в пользовательский режим, однако прерывания ещё не настроены и вернуться в ядро нельзя, так что QEMU будет просто ребутаться.
Тем не менее, с помощью gdb вы можете уже проверить что переход на пользовательский код происходит.

Если вы хотите запустить какую-то другую программу, то необязательно изменять руками код в `kern/init.c`.
В GNUMakefile определён простой способ запуска пользовательских окружений. Вместо `hello` может быть любая другая программа из `users`.

```
make run-hello
make run-hello-nox
make run-hello-gdb
make run-hello-nox-gdb
```

## Поддержка исключений

Про исключения подробно рассказывалось на четвёртой лекции, а на семинарах разбиралось как они работают в xv6.
Здесь нужно будет добавить поддержку исключений в JOS.

Добавьте точки входа для исключений в `trapentry.S`. Используйте объявления из `inc/trap.h` Вот некоторые полезные исключения:

```
Vector | Mnem. | Description         | Err. code
------------------------------------------------
0      | #DE   | Divide error        | No 
1      | #DB   | Debug exception     | No 
3      | #BP   | Breakpoint (int3)   | No 
6      | #UD   | Invalid opcode      | No 
13     | #GP   | General protection  | Yes 
14     | #PF   | Page fault          | Yes 
------------------------------------------------
```

Чтобы процессор знал где находятся обработчики их нужно записать в таблицу дескрипторов прерываний в функции `trap_init()` в `kern/trap.c`.
Для составления дескриптора используйте макрос `SETGATE` из `inc/mmu.h`

Обработка исключений должна происходить в функции `trap()` в `kern/trap.c`. Функция `trap()` принимает на вход указатель на `struct Trapframe`.
Аппаратура сохраняет далеко не все регистры, вам нужно подготовить Trapframe в обработчике прерываний.
Посмотрите на структуру Trapframe в `inc/trap.h`. Всё, что ниже `tr_err` будет лежать на стеке при входе в прерывание.
Для некоторых прерываний на стеке также будет лежать код ошибки.
В тех случаях, когда кода ошибки нет, стоит записать на стек 0, чтобы было однообразно. Можно для этого объявить точку входа через макрос `TRAPHANDLER_NOEC` (он определён в самом `trapentry.S`). Также макросы `TRAPHANDLER_NOEC` и `TRAPHANDLER` положат на стек номер прерывания, что соответствует элементу `tf_trapno` структуры Trapframe. Оставшиеся элементы структуры Trapframe общие для всех прерываний и удобно написать код их инициализации один раз в `_alltraps`:
1. Сохраните на стек значения регистров `%ds` и `%es`
2. Положите значения general-purpose регстров на стек инструкцией `pushal`

Теперь осталось передать управления в `trap()`. Напомним, что при входе в прерывание аппаратура проинициализировала сегменты CS и SS, но сегмены DS и ES остались пользовательскими, так что перед переходом нужно их проинициализировать. Доделайте `_alltraps`:

1. Загрузите `GD_KD` в `%ds` и `%es` (воспользуйтесь регистром `%ax`, потому как напрямую писать константу в селектор сегмента нельзя)
2. Выполните `pushl %esp`, чтобы положить на стек указатель на `struct Trapframe`, который потом передать в `trap()` (напомним, в 32-битном x86 аргументы передаются через стек)
3. Позовите `trap()`

Теперь у вас заработают программы которые приводят к исключениями, например `users/divzero`.

Запустите `make grade`. У вас должны пройти тесты `divzero`, `softint`, и `badsegment`.

## Поддержка Page Fault

Поправьте `trap_dispatch()` так, чтобы при page fault вызывалась `page_fault_handler()`. Должны пройти тесты `faultread`, `faultreadkernel`, `faultwrite`, и `faultwritekernel` (при запуске `make grade`).

## Поддержка Breakpoint

Поправьте `trap_dispatch()` чтобы в случае брейкпойнта запускался `monitor()` из `kern/monitor.c`. Должен заработать тест `breakpoint` (при запуске `make grade`).

## Поддержка Системных вызовов

В JOS системому вызову соответствует вектор 40. Чтобы не запоминать, стоит пользоваться `T_SYSCALL`, объявленной в `inc/trap.h`.
Аргументы системного вызова передаются на регистрах. Посмотрите на функцию `syscall()` в `lib/syscall.c` чтобы понять интерфейс.
Добавьте обработчик `T_SYSCALL` в `trapentry.S` и `trap_init()`. В `trap_dispatch()` позовите `syscall()` из `kern/syscall.c` с правильными аргументами.
Наконец, вам нужно будет реализовать эту самую  `syscall()`.

Если вы всё сделаете правильно, то программа `hello` должна напечатать "hello, world" и свалиться в page fault. Также должен заработать тест `testbss`

## Инициализация пользовательского окружения

Чтобы заработала `hello` нужно добавить код в `lib/libmain.c`, чтобы глобальная переменная `thisenv` указывала на соответствующую `Env` в ядерном массиве `envs` (вспомните, что мы делали его доступным на чтение в пользовательском коде). Номер текущего `Env` можно получить с помощью `sys_getenvid()`.

После этого наконце заработает тест `hello`.

## Page Fault и защита памяти

Как правило операционная система пологается на железо, когда речь идёт о защите памяти. Но в системных вызовах нужно быть очень аккуратным: с одной стороны, при обращении к неинициализированной памяти можно получить page fault, который непонятно как обрабатывать. С другой - ядру доступно больше памяти и права доступа нужно проверять самостоятельно.

Модифицируйте `kern/trap.c` чтобы при page fault в режиме ядра происходил вызов `panic()`.

Теперь перейдём к проверке прав доступа. Напишите реализацию `user_mem_check()` в `kern/pmap.c`. Поправьте `kern/syscall.c` чтобы проверять доступ к памяти с помощью `user_mem_assert()`.

Теперь у вас должны проходить все тесты из `make grade`. Задача будет засчитана только когда пройдут все тесты.
