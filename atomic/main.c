#include <stdio.h>
#include <pthread.h>
#include <assert.h>
#include "atomic.h"

#define NITER (1<<16)
#define NTHREADS 1000

void* summer1 (void *arg)
{
    int i;
    for (i = 0; i < NITER; ++i) {
        atomic_add((long*) arg, 1);
    }
    return 0;
}

void* summer2 (void *arg)
{
    int i;
    for (i = 0; i < NITER; ++i) {
        atomic_add((long*) arg, 2);
    }
    return 0;
}

void* subber1 (void *arg)
{
    int i;
    for (i = 0; i < NITER; ++i) {
        atomic_sub((long*) arg, 1);
    }
    return 0;
}

void* subber2 (void *arg)
{
    int i;
    for (i = 0; i < NITER; ++i) {
        atomic_sub((long*) arg, 2);
    }
    return 0;
}

struct xchg_test
{
    long flag;
    long counter;
};

void* xchg_tester(void *arg)
{
    struct xchg_test *x = (struct xchg_test*) arg;
    while (!atomic_xchg(&x->flag, 0))
        ;
    x->counter++;
    asm volatile("movq $1, %0" : "=m" (x->flag));
    return 0;
}

void* cmpxchg_tester(void *arg)
{
    long *l = (long*) arg;
    long e = *l, n;
    do {
        n = e & 1 ? e + 1 : e + 3;
    } while (!atomic_cmpxchg(l, &e, n));
}

void run_threads(void* (*f) (void*), void *arg)
{
    int i;
    pthread_t tid[NTHREADS];
    for (i = 0; i < NTHREADS; i++){
        pthread_create(&tid[i], NULL, f, arg);
    }
    for (i = 0; i < NTHREADS; i++){
        pthread_join(tid[i], NULL);
    }
}

int main()
{
    long c = 0;
    run_threads(summer1, &c);
    assert(c == (long) NITER * NTHREADS);
    printf("Passed summer1 test\n");

    run_threads(subber1, &c);
    assert(c == 0);
    printf("Passed subber1 test\n");

    run_threads(summer2, &c);
    assert(c == (long) 2 * NITER * NTHREADS);
    printf("Passed summer2 test\n");

    run_threads(subber2, &c);
    assert(c == 0);
    printf("Passed subber2 test\n");

    int i;
    for (i = 0; i < 100; i++) {
        struct xchg_test x = {1, 0};
        run_threads(xchg_tester, &x);
        assert(x.counter == NTHREADS);
    }
    printf("Passed xchg test\n");

    for (i = 0; i < 100; i++) {
        long x = 1;
        run_threads(cmpxchg_tester, &x);
        assert(x == 2 * NTHREADS + (NTHREADS % 2 == 0));
    }
    printf("Passed cmpxchg test\n");

    return 0;
}
